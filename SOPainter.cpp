#include "SOPainter.h"

#include <QPainter>
#include <QtScript>
#include <QDebug>

SOPainter::SOPainter(QObject *parent) :
	QObject(parent),
	p(new QPainter)
{
}

SOPainter::~SOPainter()
{
	delete p;
}

void SOPainter::begin(int width, int height)
{
	if (p->isActive())
		p->end();

	mCurrentImage = QImage(width, height, QImage::Format_ARGB32_Premultiplied);
	mCurrentImage.fill(Qt::transparent);

	p->begin(&mCurrentImage);
	p->setRenderHint(QPainter::Antialiasing);
	p->setBrush(Qt::black);
}

void SOPainter::push()
{
	p->save();
}

void SOPainter::pop()
{
	p->restore();
}

void SOPainter::scale(qreal sx, qreal sy)
{
	p->scale(sx, context()->argumentCount() > 1 ? sy : sx);
}

void SOPainter::rotate(qreal degrees)
{
	p->rotate(degrees);
}

void SOPainter::translate(qreal dx, qreal dy)
{
	p->translate(dx, dy);
}

void SOPainter::setFill(QScriptValue color)
{
	painter()->setBrush(colorFromScriptValue(color));
}

void SOPainter::end()
{
	if (p->isActive())
		p->end();
}

void SOPainter::rect(double x, double y, double width, double height)
{
	p->fillRect(QRectF(x, y, width, height), p->brush());
}

QColor SOPainter::colorFromScriptValue(QScriptValue v)
{
	qreal rgba[4] = { 0, 0, 0, 1 };

	if (v.isArray()) {
		QVariantList ls = v.toVariant().toList();
		int iMax = qMin(4, ls.count());
		for(int i = 0; i < iMax; ++i) {
			rgba[i] = ls[i].toReal();
		}
	}
	else {
		rgba[0] = v.property("r").toNumber();
		rgba[1] = v.property("g").toNumber();
		rgba[2] = v.property("b").toNumber();
		rgba[3] = v.property("a").toNumber();
	}

	return QColor::fromRgbF(rgba[0], rgba[1], rgba[2], rgba[3]);
}
