#ifndef DOCUMENTMANAGER_H
#define DOCUMENTMANAGER_H

#include <QObject>

#include "iDocumentManagerDelegate.h"

class QFileDialog;
class QWidget;

class DocumentManager : public QObject
{
	Q_OBJECT
public:
	explicit DocumentManager(iDocumentManagerDelegate* delegate, QWidget* documentWindow = 0);
	
	iDocumentManagerDelegate* delegate() const
	{ return mDelegate; }

	QWidget* documentWindow() const
	{ return mDocumentWindow; }

	bool documentIsModified() const
	{ return mDocumentIsModified; }

	QString documentFilePath() const
	{ return mDocumentFilePath; }

	bool setDocumentFilePath(const QString& filePath);
	bool saveModifications();
	bool eventFilter(QObject *o, QEvent *e);

signals:
	void documentSaved();

public slots:
	void setDocumentIsModified(bool modified);
	bool openFile();
	bool saveFile();
	bool saveFileAs();

protected slots:
	void saveFileDialog();

protected:
	void setupFileDialog(QFileDialog* dlg);
	bool writeDocumentToFile();

private:
	iDocumentManagerDelegate* mDelegate;
	QWidget*	mDocumentWindow;
	bool		mDocumentIsModified;
	QString		mDocumentFilePath;
};

#endif // DOCUMENTMANAGER_H
