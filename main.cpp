#include <QtGui/QApplication>
#include <QtScript>
#include <QDebug>

#include "MainWindow.h"

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	a.setApplicationName("QScriptTest");
	a.setOrganizationName("fschehnSOFT");
	a.setApplicationVersion("0.0.0");

	MainWindow w;
	w.show();

	return a.exec();
}

//#include <QtGui/QApplication>
//#include <QtScript>
//#include <QDebug>
//#include <QColor>

//#include "SOColor.h"

//#define trace(x) qDebug() << #x << " = " << (x);

//int main(int argc, char *argv[])
//{
//	QApplication a(argc, argv);
//	Q_UNUSED(a);

//	QScriptEngine e;
//	SOColor::initialize(&e);
//	QScriptValue g = e.globalObject();

////	g.setProperty("Color", SOColor::prototype());

//	QString program(
//				"print('>>>>>');"
//				"var c = new Color(0.1, 0.2, 0.3, 0.4);"
//				"print(Color);"
//				"print(c);"
//				"c.red = 0.95;"
//				"print(c.red);"
//				"print('<<<<<');"
//				);

//	QScriptValue result = e.evaluate(program);
//	Q_UNUSED(result);

//	QColor c = qscriptvalue_cast<QColor>(g.property("c"));

//	qDebug() << "c = " << c;

//	return 0;
//}
