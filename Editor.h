#ifndef EDITOR_H
#define EDITOR_H

#include <QPlainTextEdit>

class Editor : public QPlainTextEdit
{
	Q_OBJECT
public:
	explicit Editor(QWidget *parent = 0);
	
public slots:
	void toggleComment();

protected:
	virtual void keyPressEvent(QKeyEvent *e);
};

#endif // EDITOR_H
