#ifndef SOPAINTER_H
#define SOPAINTER_H

#include <QObject>
#include <QScriptable>
#include <QImage>
#include <QStack>

class QPainter;
class QImage;

class SOPainter : public QObject, protected QScriptable
{
	Q_OBJECT

public:
	explicit SOPainter(QObject *parent = 0);
	virtual ~SOPainter();

	QPainter* painter() const
	{ return p; }

	QImage currentImage() const
	{ return mCurrentImage; }

	void end();

	Q_INVOKABLE void begin(int width, int height);
	Q_INVOKABLE void push();
	Q_INVOKABLE void pop();
	Q_INVOKABLE void scale(qreal sx, qreal sy = 0.0);
	Q_INVOKABLE void translate(qreal dx, qreal dy = 0.0);
	Q_INVOKABLE void rotate(qreal degrees);
	Q_INVOKABLE void setFill(QScriptValue color);
	Q_INVOKABLE void rect(double x, double y, double width, double height);

public:
	static QColor colorFromScriptValue(QScriptValue v);

private:
	QPainter*	p;
	QImage		mCurrentImage;
};

#endif // SOPAINTER_H
