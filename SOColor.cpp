#include "SOColor.h"

SOColor::SOColor(QObject *parent) :
	QObject(parent),
	QColor(0, 0, 0, 0)
{
}

void SOColor::initialize(QScriptEngine *e)
{
	QScriptValue prototype = e->newFunction(constructor);
//	e->setDefaultPrototype(qMetaTypeId<SOColor>(), prototype);
	e->setDefaultPrototype(qMetaTypeId<SOColor*>(), prototype);
	e->globalObject().setProperty("Color", prototype);
}

QScriptValue SOColor::constructor(QScriptContext *c, QScriptEngine *e)
{
	qreal r = c->argument(0).toNumber();
	qreal g = c->argument(1).toNumber();
	qreal b = c->argument(2).toNumber();
	qreal a = c->argumentCount() > 3 ? c->argument(3).toNumber() : 1.0;

	SOColor* color = new SOColor();
	color->setRedF(r);
	color->setGreenF(g);
	color->setBlueF(b);
	color->setAlphaF(a);

	return e->toScriptValue(color);
}
