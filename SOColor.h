#ifndef SOCOLOR_H
#define SOCOLOR_H

#include <QtScript>
#include <QColor>

class SOColor;

Q_DECLARE_METATYPE(SOColor*)

class SOColor :
		public QObject,
		public QColor,
		protected QScriptable
{
	Q_OBJECT

	Q_PROPERTY(qreal red
			   READ redF
			   WRITE _setRedF
			   SCRIPTABLE true
			   STORED true
//               [USER bool]
//               [CONSTANT]
//               [FINAL]
			   )
public:
	explicit SOColor(QObject *parent = 0);
	
	static void initialize(QScriptEngine* e);
	static QScriptValue constructor(QScriptContext *c, QScriptEngine *e);

signals:
	
public slots:
	void _setRedF(qreal red)
	{ QColor::setRedF(red); }
};

#endif // SOCOLOR_H
