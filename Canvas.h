#ifndef CANVAS_H
#define CANVAS_H

#include <QWidget>
#include <QImage>

class Canvas : public QWidget
{
	Q_OBJECT
public:
	explicit Canvas(QWidget *parent = 0);
	
	QImage image() const
	{ return mImage; }

	void setImage(const QImage& image);

protected:
	virtual void paintEvent(QPaintEvent *);

private:
	QImage mImage;
};

#endif // CANVAS_H
