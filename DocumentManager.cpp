#include "DocumentManager.h"

#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <QFile>
#include <QFileInfo>
#include <QSettings>
#include <qevent.h>

#define kSettingsGroup		"DocumentManager"
#define kFileDialogState	"fileDialogState"
#define kFileDialogGeometry	"fileDialogGeometry"

DocumentManager::DocumentManager(iDocumentManagerDelegate *delegate, QWidget *documentWindow /* =0 */) :
	QObject(documentWindow),
	mDelegate(delegate),
	mDocumentWindow(documentWindow),
	mDocumentIsModified(false)
{
	Q_ASSERT(delegate);

	if (documentWindow) {
		documentWindow->installEventFilter(this);
	}
}

bool DocumentManager::openFile()
{
	if (saveModifications()) {
		QFileDialog dlg(documentWindow(), tr("Open Document..."));
		setupFileDialog(&dlg);
		dlg.setAcceptMode(QFileDialog::AcceptOpen);

		if(dlg.exec() == QFileDialog::Accepted && dlg.selectedFiles().count()) {
			return setDocumentFilePath(dlg.selectedFiles().first());
		}
	}

	return false;
}

bool DocumentManager::saveFile()
{
	if (documentFilePath().isEmpty()) {
		return saveFileAs();
	}

	return writeDocumentToFile();
}

bool DocumentManager::saveFileAs()
{
	QFileDialog dlg(documentWindow(), tr("Save Document As..."));
	setupFileDialog(&dlg);
	dlg.setAcceptMode(QFileDialog::AcceptSave);

	if (dlg.exec() != QDialog::Accepted || !dlg.selectedFiles().count())
		return false;

	mDocumentFilePath = dlg.selectedFiles().first();

	return writeDocumentToFile();
}

void DocumentManager::saveFileDialog()
{
	QFileDialog* dlg = qobject_cast<QFileDialog*>(sender());
	Q_ASSERT(dlg);

	QSettings s;
	s.beginGroup(kSettingsGroup);
	s.setValue(kFileDialogState, dlg->saveState());
	s.setValue(kFileDialogGeometry, dlg->saveGeometry());
}

void DocumentManager::setupFileDialog(QFileDialog *dlg)
{
	this->connect(dlg, SIGNAL(finished(int)), SLOT(saveFileDialog()));
	dlg->setWindowModality(Qt::WindowModal);

	QSettings s;
	s.beginGroup(kSettingsGroup);
	dlg->restoreGeometry(s.value(kFileDialogGeometry).toByteArray());
	dlg->restoreState(s.value(kFileDialogState).toByteArray());
}

bool DocumentManager::writeDocumentToFile()
{
	if (!documentFilePath().size())
		return false;

	QFile file(documentFilePath());

	if (!file.open(QIODevice::WriteOnly)) {
		QString title = tr("Error");
		QString text = tr("The file '%0' could not be opened for reading!").arg(documentFilePath());
		QMessageBox::warning(documentWindow(), title, text, QMessageBox::Ok);
	}

	return delegate()->writeDocumentToDevice(&file);
}

void DocumentManager::setDocumentIsModified(bool modified)
{
	mDocumentIsModified = modified;
	if (documentWindow()) {
		documentWindow()->setWindowModified(modified);
	}
}

bool DocumentManager::saveModifications()
{
	if (documentIsModified()) {
		QString message = tr("There are unsaved changes. Do you want do save the Document?");
		QString title = tr("Unsaved Changes");

		if (QMessageBox::Yes == QMessageBox::question(documentWindow(), title, message, QMessageBox::No, QMessageBox::Yes)) {
			if (documentFilePath().size()) {
				return saveFile();
			}
			else {
				return saveFileAs();
			}
		}
	}

	return true;
}

bool DocumentManager::eventFilter(QObject *o, QEvent *e)
{
	if (o == mDocumentWindow && e->type() == QEvent::Close) {
		((QCloseEvent*)e)->setAccepted(saveModifications());
		return true;
	}

	return QObject::eventFilter(o, e);
}

bool DocumentManager::setDocumentFilePath(const QString &filePath)
{
	mDocumentFilePath = filePath;

	if (filePath.isEmpty())
		return true;

	QFile file(filePath);

	if (!file.open(QIODevice::ReadOnly)) {
		QString text = tr("The file '%0' could not be opened for reading!").arg(documentFilePath());
		QMessageBox::warning(documentWindow(), tr("Warning"), text, QMessageBox::Ok);
	}

	if (delegate()->readDocumentFromDevice(&file)) {
		if(documentWindow())
			documentWindow()->setWindowTitle(QFileInfo(filePath).fileName());
		return true;
	}

	return false;
}
