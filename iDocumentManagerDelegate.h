#ifndef IDOCUMENTMANAGERDELEGATE_H
#define IDOCUMENTMANAGERDELEGATE_H

class QIODevice;

class iDocumentManagerDelegate
{
public:
	virtual bool writeDocumentToDevice(QIODevice* device) = 0;
	virtual bool readDocumentFromDevice(QIODevice* device) = 0;
};

#endif // IDOCUMENTMANAGERDELEGATE_H
