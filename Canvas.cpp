#include "Canvas.h"

#include <QPainter>

Canvas::Canvas(QWidget *parent) :
	QWidget(parent)
{}

void Canvas::setImage(const QImage &image)
{
	mImage = image;
	update();
}

void Canvas::paintEvent(QPaintEvent *)
{
	QPainter p(this);
	p.drawImage(0, 0, image());
}
