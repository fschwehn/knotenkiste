#include "Console.h"

#include <QDebug>
#include <QTextStream>
#include <QBuffer>

#define trace(x) qDebug() << #x << " = " << (x)


Console::Console(QWidget *parent) :
	QPlainTextEdit(parent)
{
	setFont(QFont("Menlo", 12));
	QFontMetrics fm(font());
	setTabStopWidth(fm.width(QChar(' ')) * 4);
}

Console::~Console()
{}

void Console::appendError(const QString &error)
{
	QTextCursor c = textCursor();
	QTextCharFormat f = c.charFormat();
	f.setForeground(Qt::red);
	c.setCharFormat(f);
	setTextCursor(c);

	appendPlainText(error);
}

void Console::appendText(const QString &text)
{
	QTextCursor c = textCursor();
	QTextCharFormat f = c.charFormat();
	f.setForeground(Qt::blue);
	c.setCharFormat(f);
	setTextCursor(c);

	appendPlainText(text);
}
