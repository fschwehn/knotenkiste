#-------------------------------------------------
#
# Project created by QtCreator 2012-06-01T20:54:52
#
#-------------------------------------------------

QT       += core gui script scripttools
TARGET = KnotenKiste
TEMPLATE = app
CONFIG += precompile_header
PRECOMPILED_HEADER = KnotenKiste.pch


SOURCES += main.cpp\
        MainWindow.cpp \
    ScriptEngine.cpp \
    Editor.cpp \
    SOPainter.cpp \
    DocumentManager.cpp \
    Canvas.cpp \
    _SOColor.cpp \
    SOColor.cpp \
    Console.cpp

HEADERS  += MainWindow.h \
    ScriptEngine.h \
    Editor.h \
    SOPainter.h \
    DocumentManager.h \
    iDocumentManagerDelegate.h \
    Canvas.h \
    KnotenKiste.pch \
    _SOColor.h \
    SOColor.h \
    Console.h

FORMS    += MainWindow.ui
