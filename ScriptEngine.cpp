#include "ScriptEngine.h"
#include "SOPainter.h"

#include <QDebug>
#include <QPaintDevice>
#include <QPainter>
#include <QImage>
#include <QThread>


class ScriptEngine::ScriptThread : public QThread
{
public:
	ScriptEngine*	engine;
	QString			program;

	ScriptThread(ScriptEngine* engine, const QString& program) :
		engine(engine),
		program(program)
	{}

protected:
	virtual void run()
	{
		QScriptValue result = engine->evaluate(program);
		engine->mPainter->end();

		if (result.isError()) {
			emit engine->scriptError(result.toString());
		}
		else {
			engine->emit renderedImage(engine->mPainter->currentImage());
		}

		engine->mCurrentScriptThread = 0;
		deleteLater();
	}
};

ScriptEngine::ScriptEngine(QObject *parent) :
	QScriptEngine(parent),
	mPainter(new SOPainter(this)),
	mCurrentScriptThread(0)
{
	// replace print function
	globalObject().setProperty("print", newFunction(print));

	// set painter
	QScriptValue painter = newQObject(mPainter);
	painter.setProperty("bg", qScriptValueFromValue(this, QColor(255, 128, 64)));
	globalObject().setProperty("painter", painter);

	// set random function
	globalObject().setProperty("random", newFunction(random));
}

ScriptEngine::~ScriptEngine()
{}

void ScriptEngine::executeScript(const QString &program)
{
	if (mCurrentScriptThread)
		return;

	mCurrentScriptThread = new ScriptThread(this, program);
	mCurrentScriptThread->start();
}

QScriptValue ScriptEngine::random(QScriptContext*, QScriptEngine* e)
{
	return  QScriptValue(e, rand() / (qreal)RAND_MAX);
}

QScriptValue ScriptEngine::print(QScriptContext *c, QScriptEngine *e)
{
	QString buf;

	for (int i = 0; i < c->argumentCount(); ++i) {
		buf += c->argument(i).toString();
		if (i < c->argumentCount() - 1)
			buf += ", ";
	}

	emit ((ScriptEngine*)e)->scriptOutput(buf);
	return e->undefinedValue();
}
