#ifndef SCRIPTENGINE_H
#define SCRIPTENGINE_H

#include <QScriptEngine>

class SOPainter;


class ScriptEngine : public QScriptEngine
{
	Q_OBJECT
public:
	explicit ScriptEngine(QObject *parent = 0);
	virtual ~ScriptEngine();

	void executeScript(const QString &program);

signals:
	void scriptOutput(const QString& text);
	void scriptError(const QString& text);
	void renderedImage(const QImage& image);

protected:
	class ScriptThread;

	ScriptThread* currentScriptThread() const
	{ return mCurrentScriptThread; }

	static QScriptValue print(QScriptContext * c, QScriptEngine *e);
	static QScriptValue random(QScriptContext *, QScriptEngine *e);

private:
	SOPainter*		mPainter;
	ScriptThread*	mCurrentScriptThread;
};

#endif // SCRIPTENGINE_H
