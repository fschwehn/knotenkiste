#ifndef CONSOLE_H
#define CONSOLE_H

#include <QPlainTextEdit>


class Console : public QPlainTextEdit
{
	Q_OBJECT
public:
	explicit Console(QWidget *parent = 0);
	virtual ~Console();

public slots:
	void appendError(const QString& error);
	void appendText(const QString& text);
};

#endif // CONSOLE_H
