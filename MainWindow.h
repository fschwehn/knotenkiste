#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "iDocumentManagerDelegate.h"

namespace Ui {
class MainWindow;
}

class QSplitter;
class Editor;
class ScriptEngine;
class DocumentManager;

class MainWindow :
		public QMainWindow,
		public iDocumentManagerDelegate
{
	Q_OBJECT
	
public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

	virtual bool writeDocumentToDevice(QIODevice* device);
	virtual bool readDocumentFromDevice(QIODevice* device);
	
public slots:
	void executeScript();

protected slots:
	void scriptExecuted(const QImage& renderedImage);

private:
	Ui::MainWindow *ui;

	ScriptEngine*			mScriptEngine;
	DocumentManager*		mDocManager;
};

#endif // MAINWINDOW_H
