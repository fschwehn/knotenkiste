#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "Editor.h"
#include "ScriptEngine.h"
#include "DocumentManager.h"

#include <QSettings>
#include <QDebug>
#include <QFile>

#define KeyGroupName				"mainWindow"
#define KeyState					"state"
#define KeyGeometry					"geometry"
#define KeyMainSplitterState		"rightSplitterState"
#define KeyMainSplitterGeometry		"rightSplitterGeometry"
#define KeyRightSplitterState		"leftSplitterState"
#define KeyRightSplitterGeometry	"leftSplitterGeometry"
#define KeyResentFile				"resentFile"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
	ui(new Ui::MainWindow),
	mScriptEngine(new ScriptEngine)
{
	// setup this
	ui->setupUi(this);
	connect(ui->actionRun_Script, SIGNAL(triggered()), this, SLOT(executeScript()));

	// setup actions
	ui->actionOpen->setShortcut(QKeySequence::Open);
	ui->actionSave->setShortcut(QKeySequence::Save);
	ui->actionSave_As->setShortcut(QKeySequence::SaveAs);
	ui->actionToggle_Comment->setShortcut(QKeySequence("CTRL+/"));

	// setup script engine
	connect(mScriptEngine, SIGNAL(scriptOutput(QString)), ui->console, SLOT(appendText(QString)));
	connect(mScriptEngine, SIGNAL(scriptError(QString)), ui->console, SLOT(appendError(QString)));
	connect(mScriptEngine, SIGNAL(renderedImage(QImage)), this, SLOT(scriptExecuted(QImage)));

	// setup canvas
	ui->canvas->installEventFilter(this);

	// setup editor
	connect(ui->actionToggle_Comment, SIGNAL(triggered()), ui->editor, SLOT(toggleComment()));

	// setup doc manager
	mDocManager = new DocumentManager(this, this);

	connect(ui->actionOpen, SIGNAL(triggered()), mDocManager, SLOT(openFile()));
	connect(ui->actionSave, SIGNAL(triggered()), mDocManager, SLOT(saveFile()));
	connect(ui->actionSave_As, SIGNAL(triggered()), mDocManager, SLOT(saveFileAs()));
	connect(ui->editor, SIGNAL(modificationChanged(bool)), mDocManager, SLOT(setDocumentIsModified(bool)));

	// restore settings
	QSettings s;
	s.beginGroup(KeyGroupName);

	restoreGeometry(s.value(KeyGeometry).toByteArray());
	restoreState(s.value(KeyState).toByteArray());

	ui->mainSplitter->restoreGeometry(s.value(KeyMainSplitterGeometry).toByteArray());
	ui->mainSplitter->restoreState(s.value(KeyMainSplitterState).toByteArray());

	ui->rightSplitter->restoreGeometry(s.value(KeyRightSplitterGeometry).toByteArray());
	ui->rightSplitter->restoreState(s.value(KeyRightSplitterState).toByteArray());

	mDocManager->setDocumentFilePath(s.value(KeyResentFile).toString());
}

MainWindow::~MainWindow()
{
	// save settings
	QSettings s;
	s.beginGroup(KeyGroupName);

	s.setValue(KeyState, saveState());
	s.setValue(KeyGeometry, saveGeometry());

	s.setValue(KeyMainSplitterState, ui->mainSplitter->saveState());
	s.setValue(KeyMainSplitterGeometry, ui->mainSplitter->saveGeometry());

	s.setValue(KeyRightSplitterState, ui->rightSplitter->saveState());
	s.setValue(KeyRightSplitterGeometry, ui->rightSplitter->saveGeometry());

	s.setValue(KeyResentFile, mDocManager->documentFilePath());

	delete ui;
}

bool MainWindow::writeDocumentToDevice(QIODevice *device)
{
	QTextStream stream(device);
	stream << ui->editor->toPlainText();
	ui->editor->document()->setModified(false);
	return true;
}

bool MainWindow::readDocumentFromDevice(QIODevice *device)
{
	ui->editor->setPlainText(QTextStream(device).readAll());
	return true;
}

void MainWindow::executeScript()
{
	ui->console->clear();
	mScriptEngine->executeScript(ui->editor->toPlainText());
}

void MainWindow::scriptExecuted(const QImage &renderedImage)
{
	ui->canvas->setImage(renderedImage);
}


