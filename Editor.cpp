#include "Editor.h"

#include <QDebug>

#define trace(x) qDebug() << #x << " = " << (x)

Editor::Editor(QWidget *parent) :
    QPlainTextEdit(parent)
{
	setFont(QFont("Menlo", 12));
	QFontMetrics fm(font());
	setTabStopWidth(fm.width(QChar(' ')) * 4);
}

void Editor::toggleComment()
{
	QTextCursor oldCursor	= textCursor();
	int selectionStart		= oldCursor.selectionStart();

	// find start of first line within selection
	QTextCursor c = oldCursor;
	if(c.hasSelection()) {
		c.setPosition(selectionStart);
		c.movePosition(QTextCursor::StartOfLine);
	}

	// start editing
	c.beginEditBlock();

	// iterate over lines
	static QRegExp rx("^[^/]*(//)(.*)$");

	QTextBlock b = c.block();
	bool doComment = !rx.exactMatch(b.text());

	do {
		c.setPosition(b.position());

		if (doComment) {
			c.insertText("//");
		}
		else {
			if (rx.exactMatch(b.text())) {
				int pos = b.position() + rx.pos(1);
				c.setPosition(pos);
				c.setPosition(pos + 2, QTextCursor::KeepAnchor);
				c.removeSelectedText();
			}
		}

		b = b.next();
	} while(b.position() < oldCursor.selectionEnd());

	// restore selection and end editing
	c.setPosition(selectionStart);
	c.setPosition(oldCursor.selectionEnd(), QTextCursor::KeepAnchor);
	c.endEditBlock();

	setTextCursor(c);
}

void Editor::keyPressEvent(QKeyEvent *e)
{
	return QPlainTextEdit::keyPressEvent(e);
}
